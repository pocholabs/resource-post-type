<?php

/**
 * Fired during plugin deactivation.
 *
 * @link       http://example.com
 * @since      1.0.0
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 *
 * @author     Your Name <email@example.com>
 */
class Resource_Post_Type_Deactivator
{
    /**
     * Short Description. (use period).
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function deactivate()
    {
    }
}
