<?php

/**
 * Fired during plugin activation.
 *
 * @link       http://example.com
 * @since      1.0.0
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 *
 * @author     Your Name <email@example.com>
 */
class Resource_Post_Type_Activator
{
    /**
     * Short Description. (use period).
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {
    }
}
