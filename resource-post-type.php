<?php

/**
 * Plugin Name: Resource Post Type
 * Plugin URI: http://pocholabs.com
 * Description: Create a custom post type for Resources. Custom Plugin for fvaldezlaw.com
 * Version: 1.0.0
 * Author: PochoLabs
 * Author URI: http://pocholabs.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Bitbucket Plugin URI: https://bitbucket.org/pocholabs/resource-post-type
 * Bitbucket Branch: master
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-resource-post-type-activator.php.
 */
function activate_resource_post_type()
{
    require_once plugin_dir_path(__FILE__).'includes/class-resource-post-type-activator.php';
    Resource_Post_Type_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-resource-post-type-deactivator.php.
 */
function deactivate_resource_post_type()
{
    require_once plugin_dir_path(__FILE__).'includes/class-resource-post-type-deactivator.php';
    Resource_Post_Type_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_resource_post_type');
register_deactivation_hook(__FILE__, 'deactivate_resource_post_type');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__).'includes/class-resource-post-type.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_resource_post_type()
{
    $plugin = new Resource_Post_Type();
    $plugin->run();
}
run_resource_post_type();
